#include "../common/book.h"
#include<sys/time.h>

#define SIZE (1024*1024*100)


__global__ void histo_kernel(unsigned char *buffer, long size, unsigned int *histo){
	__shared__ unsigned int temp[256];
	temp[threadIdx.x] = 0;
	__syncthreads();

	int i = threadIdx.x + blockDim.x * blockIdx.x;
	int stride = blockDim.x * gridDim.x;
	while( i < size ){
		atomicAdd( &(temp[buffer[i]]), 1 );
		i += stride;
	}
	__syncthreads();
	
	atomicAdd( &(histo[threadIdx.x]), temp[threadIdx.x] );
}

int main(){
	cudaEvent_t start, stop;
	float t;
	unsigned char *buffer = (unsigned char*)big_random_block( SIZE );
	unsigned int histo[256];
	unsigned char *dev_buffer;
	unsigned int *dev_histo;

	HANDLE_ERROR( cudaMalloc( (void**)&dev_buffer, SIZE ) );
	HANDLE_ERROR( cudaMemcpy( dev_buffer, buffer, SIZE, cudaMemcpyHostToDevice) );
	HANDLE_ERROR( cudaMalloc( (void**)&dev_histo, sizeof(int) * 256 ) );
	HANDLE_ERROR( cudaMemset( dev_histo, 0, 256 * sizeof( int ) ) );

	cudaDeviceProp prop;
	HANDLE_ERROR( cudaGetDeviceProperties(&prop, 0) );
	int blocks = prop.multiProcessorCount;
	HANDLE_ERROR( cudaEventCreate( &start ) );
	HANDLE_ERROR( cudaEventCreate( &stop ) );
	HANDLE_ERROR( cudaEventRecord( start, 0 ) );

	histo_kernel<<<blocks * 2, 256>>>(dev_buffer, SIZE, dev_histo);

	HANDLE_ERROR( cudaEventRecord(stop, 0) );
	HANDLE_ERROR( cudaEventSynchronize(stop) );
	HANDLE_ERROR( cudaEventElapsedTime(&t, start, stop) );

	printf("Elapsed time on GPU: %.6f\n", t);
	
	HANDLE_ERROR( cudaMemcpy( histo, dev_histo, 256*sizeof(int), cudaMemcpyDeviceToHost ) );

	long histoCount = 0;
	for( int i = 0; i < 256; i++ )
		histoCount += histo[i];
	printf("Sum of histo: %ld\n", histoCount);	
	
	HANDLE_ERROR( cudaEventRecord( start, 0 ) );	
	for( int i = 0; i < SIZE; i++ )
		histo[buffer[i]]--;
	HANDLE_ERROR( cudaEventRecord(stop, 0) );
	HANDLE_ERROR( cudaEventSynchronize(stop) );
	HANDLE_ERROR( cudaEventElapsedTime(&t, start, stop) );
	printf("Elapsed time on CPU: %.6f\n", t);

	for( int i = 0; i < 256; i++ )
		if (histo[i] != 0)
			printf("Error in column %d!\n", i);
	HANDLE_ERROR( cudaEventDestroy(start) );
	HANDLE_ERROR( cudaEventDestroy(stop) );
	cudaFree(dev_buffer);
	cudaFree(dev_histo);
	free( buffer );
	return 0;
}

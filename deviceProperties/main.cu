#include "../common/book.h"

int main(){
	cudaDeviceProp prop;
	int count;
	HANDLE_ERROR( cudaGetDeviceCount( &count ) );
	for( int i = 0; i < count; i++ ){
		HANDLE_ERROR( cudaGetDeviceProperties( &prop, i ) );
		printf("Name: %s\n", prop.name);
		printf("Computing capabilities: %d.%d\n", prop.major, prop.minor);
	}
	return 0;
}

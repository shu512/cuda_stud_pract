#include "./common/book.h"

__global__ void experiment(int *a){
    printf("I`m a Thread num %d in block num %d. Grid size equal %d\n", threadIdx.x, blockIdx.x, blockDim.x);
    if ( (threadIdx.x == 0) && (blockIdx.x == 0) )
        printf("a[5] = %d\n", a[5]);
}

int main(){
    int i, n = 10;
    int a[n];
    int *dev_a;
    for( i = 0; i < n; i++ )
        a[i] = i;
    HANDLE_ERROR( cudaMalloc((void**)&dev_a, n * sizeof(int)) );
    HANDLE_ERROR( cudaMemcpy( dev_a, a, n * sizeof(int), cudaMemcpyHostToDevice ) );
    
    experiment<<<3,2>>>(dev_a);

    cudaFree(dev_a);
    return 0;
}

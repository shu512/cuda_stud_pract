#include "../common/book.h"
#include <stdlib.h>

const int threadsPerBlock = 32;
const int blocksPerGrid = 16;
const int n = threadsPerBlock * blocksPerGrid;

void dgemm_def_host(double *a, double *b, double *c) {
    int i, j, k;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            for (k = 0; k < n; k++) {
                c[i*n + j] += a[i*n+k] * b[k*n+j];              
            }
        }
    }
}

__global__ void dgemm_def(double *a, double *b, double *c) {
    int tid, j, k;
    tid = blockIdx.x * blockDim.x + threadIdx.x;
    while( tid < n ){
        for (j = 0; j < n; j++) {
            for (k = 0; k < n; k++) {
                c[tid*n + j] += a[tid*n+k] * b[k*n+j];              
            }
        }
        tid += blockDim.x * gridDim.x;
    }
}

void init_matrix(double *a, float value){
    int i, j;
    for (i = 0; i < n; i++)
        for (j = 0; j < n; j++)
            a[i*n+j] = value;                                   
}

int main(){
    cudaEvent_t start, stop;
    double *a, *b, *c, *c2;
    double *dev_a, *dev_b, *dev_c;
    a = (double*)malloc(sizeof(double*)*n*n);
    c2 = (double*)malloc(sizeof(double*)*n*n);
    b = (double*)malloc(sizeof(double*)*n*n);
    c = (double*)malloc(sizeof(double*)*n*n);
    HANDLE_ERROR( cudaMalloc((void**)&dev_a, sizeof(double*)*n*n) );    
    HANDLE_ERROR( cudaMalloc((void**)&dev_b, sizeof(double*)*n*n) );    
    HANDLE_ERROR( cudaMalloc((void**)&dev_c, sizeof(double*)*n*n) );    

    init_matrix(*&a, 1.0);
    init_matrix(*&b, 2.0);
    init_matrix(*&c, 0.0);
    
    HANDLE_ERROR( cudaMemcpy( dev_a, a, (sizeof(double*)*n*n), cudaMemcpyHostToDevice ));
    HANDLE_ERROR( cudaMemcpy( dev_b, b, (sizeof(double*)*n*n), cudaMemcpyHostToDevice ));
    HANDLE_ERROR( cudaMemcpy( dev_c, c, (sizeof(double*)*n*n), cudaMemcpyHostToDevice ));

    HANDLE_ERROR(cudaEventCreate(&start));
    HANDLE_ERROR(cudaEventCreate(&stop));
    HANDLE_ERROR(cudaEventRecord(start, 0));

    dgemm_def<<<blocksPerGrid,threadsPerBlock>>>( dev_a, dev_b, dev_c );

    HANDLE_ERROR(cudaEventRecord(stop, 0));
    HANDLE_ERROR(cudaEventSynchronize(stop));
    float elapsedTime;
    HANDLE_ERROR(cudaEventElapsedTime(&elapsedTime, start, stop));
    printf("Elapsed time = %3.1f\n", elapsedTime);

    HANDLE_ERROR( cudaMemcpy(c2, dev_c, sizeof(double)*n*n, cudaMemcpyDeviceToHost)); 

    //проверка корректности вычисленных значений
    dgemm_def_host(a, b, c);
    int i, j;
    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++)
        {
            if (c[i*n+j] != c2[i*n+j]) 
            {
                printf("Not equals in [%d][%d]!\n", i, j);
            }
        }
    }

    
    HANDLE_ERROR( cudaEventDestroy(start) );
    HANDLE_ERROR( cudaEventDestroy(stop) );
    cudaFree(dev_a);
    cudaFree(dev_b);
    cudaFree(dev_c);
    free(a);
    free(b);
    free(c);
    return 0;

}

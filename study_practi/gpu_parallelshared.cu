#include <stdlib.h>
#include "../common/book.h"

#define BLOCK_SIZE 16
#define N 16*32

void dgemm_def_host(float *a, float *b, float *c) {
    int i, j, k;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            for (k = 0; k < N; k++) {
                c[i*N + j] += a[i*N+k] * b[k*N+j];
            }
       }
    }
}

void init_matrix(float *a, float value){
    int i, j;
    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++)
            a[i*N+j] = value;
}


__global__ void dgemm_def ( float* a, float* b, int n, float* c ) 
{ 
    int bx = blockIdx.x;  
    int by = blockIdx.y; 
    int tx = threadIdx.x;
    int ty = threadIdx.y; 
    float sum = 0.0f; 
    int ia = n * BLOCK_SIZE * by + n * ty;  
    int ib = BLOCK_SIZE * bx + tx; 
    int ic = n * BLOCK_SIZE * by+ BLOCK_SIZE * bx; 
    for( int k = 0; k < n; k++ )// вычисляем элемент
        sum += a [ia + k] * b [ib + k*n];  
    c [ic + n * ty + tx] = sum; // запоминаем результат
}


int main()
{
    cudaEvent_t start, stop;
    float *a, *b, *c, *c2;
    a = (float*)malloc(sizeof(float*)*N*N);
    b = (float*)malloc(sizeof(float*)*N*N);
    c = (float*)malloc(sizeof(float*)*N*N);
    c2 = (float*)malloc(sizeof(float*)*N*N);
    
    init_matrix(*&a, 1.0);
    init_matrix(*&b, 2.0);
    init_matrix(*&c, 0.0);
    int numBytes = N * N * sizeof( float); 
    float* adev, * bdev, * cdev;
    dim3 threads( BLOCK_SIZE, BLOCK_SIZE ); 
    dim3 blocks( N / threads.x, N / threads.y); 
    HANDLE_ERROR(cudaMalloc( (void**)&adev, numBytes));
    HANDLE_ERROR(cudaMalloc( (void**)&bdev, numBytes));
    HANDLE_ERROR(cudaMalloc( (void**)&cdev, numBytes));

    HANDLE_ERROR(cudaMemcpy( adev, a, numBytes, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy( bdev, b, numBytes, cudaMemcpyHostToDevice));
    
    HANDLE_ERROR(cudaEventCreate(&start));
    HANDLE_ERROR(cudaEventCreate(&stop));
    HANDLE_ERROR(cudaEventRecord(start, 0));

    // вызвать ядро
    dgemm_def<<<blocks, threads>>> ( adev, bdev, N, cdev); 

    HANDLE_ERROR(cudaEventRecord(stop, 0));
    HANDLE_ERROR(cudaEventSynchronize(stop));
    float elapsedTime;
    HANDLE_ERROR(cudaEventElapsedTime(&elapsedTime, start, stop));
    printf("Elapsed time = %3.1f\n", elapsedTime);

    cudaThreadSynchronize();// дождатьяокончания расчета
    HANDLE_ERROR(cudaMemcpy( c2, cdev, numBytes, cudaMemcpyDeviceToHost));
    
    //проверка корректности вычисленных значений
    dgemm_def_host(a, b, c);
    int i, j;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++)
        {
            if (c[i*N+j] != c2[i*N+j])
            {
                printf("Not equals in [%d][%d]!\n", i, j);
                i = N;
                break;
            }
        }
    }

    HANDLE_ERROR( cudaEventDestroy(start) );
    HANDLE_ERROR( cudaEventDestroy(stop) );
    cudaFree( adev); // освободить памятьGPU
    cudaFree( bdev); 
    cudaFree( cdev); 
    free(a);
    free(b);
    free(c);
    return 0;
}

#include "../common/book.h"
#include <stdlib.h>

__global__ void dgemm_def(double *a, double *b, double *c, int n) {
    int i, j, k;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            for (k = 0; k < n; k++) {
                c[i*n + j] += a[i*n+k] * b[k*n+j];              
            }
        }
    }
}

void init_matrix(double *a, int n, float value){
    int i, j;
    for (i = 0; i < n; i++)
        for (j = 0; j < n; j++)
            a[i*n+j] = value;                                   
}

int main(){
    cudaEvent_t start, stop;
    double *a, *b, *c;
    double *dev_a, *dev_b, *dev_c;
    int i, n = 512;
    a = (double*)malloc(sizeof(double*)*n*n);
    b = (double*)malloc(sizeof(double*)*n*n);
    c = (double*)malloc(sizeof(double*)*n*n);
    HANDLE_ERROR( cudaMalloc((void**)&dev_a, sizeof(double*)*n*n) );    
    HANDLE_ERROR( cudaMalloc((void**)&dev_b, sizeof(double*)*n*n) );    
    HANDLE_ERROR( cudaMalloc((void**)&dev_c, sizeof(double*)*n*n) );    

    init_matrix(*&a, n, 1.0);
    init_matrix(*&b, n, 2.0);
    init_matrix(*&c, n, 0.0);
    
    HANDLE_ERROR( cudaMemcpy( dev_a, a, (sizeof(double*)*n*n), cudaMemcpyHostToDevice ));
    HANDLE_ERROR( cudaMemcpy( dev_b, b, (sizeof(double*)*n*n), cudaMemcpyHostToDevice ));
    HANDLE_ERROR( cudaMemcpy( dev_c, c, (sizeof(double*)*n*n), cudaMemcpyHostToDevice ));

    HANDLE_ERROR(cudaEventCreate(&start));
    HANDLE_ERROR(cudaEventCreate(&stop));
    HANDLE_ERROR(cudaEventRecord(start, 0));

    dgemm_def<<<1,1>>>( dev_a, dev_b, dev_c, n );

    HANDLE_ERROR(cudaEventRecord(stop, 0));
    HANDLE_ERROR(cudaEventSynchronize(stop));
    float elapsedTime;
    HANDLE_ERROR(cudaEventElapsedTime(&elapsedTime, start, stop));
    printf("Elapsed time = %3.1f\n", elapsedTime);

/*    HANLDE_ERROR( cudaMemcpy(&a, dev_a, sizeof(double)*n*n), cudaMemcpyDeviceToHost );
    HANLDE_ERROR( cudaMemcpy(&b, dev_b, sizeof(double)*n*n), cudaMemcpyDeviceToHost );
    HANLDE_ERROR( cudaMemcpy(&c, dev_c, sizeof(double)*n*n), cudaMemcpyDeviceToHost );  */

    HANDLE_ERROR( cudaEventDestroy(start) );
    HANDLE_ERROR( cudaEventDestroy(stop) );
    cudaFree(dev_a);
    cudaFree(dev_b);
    cudaFree(dev_c);
    free(a);
    free(b);
    free(c);
    return 0;

}

#include "../common/book.h"

#define N 128

__global__ void dgemm_def(double *a, double *b, double *c) {
    int i, j, k;

    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            for (k = 0; k < N; k++) {
                *(c+i*N + j) += *(a+i*N+k) * *(b+k*N+j);              
            }
        }
    }
}

void init_matrix(double *a, float value){
    int i, j;
    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++)
            *(a+i*N+j) = value;
}

int main(){
    cudaEvent_t start, stop;
    double a[N*N], b[N*N], c[N*N];
    double *dev_a, *dev_b, *dev_c;
    int i;
    HANDLE_ERROR( cudaMalloc((void**)&dev_a, sizeof(double)*N*N) );    
    HANDLE_ERROR( cudaMalloc((void**)&dev_b, sizeof(double)*N*N) );    
    HANDLE_ERROR( cudaMalloc((void**)&dev_c, sizeof(double)*N*N) );    
//    init_matrix(*&a, 1.0);
//    init_matrix(*&b, 2.0);
//    init_matrix(*&c, 0.0);
    for( i = 0; i < N; i++)
        for(int j = 0; j < N; j++){
            a[i*N+j] = 1.0;
            b[i*N+j] = 2.0;
            c[i*N+j] = 0.0;
        }
    
    HANDLE_ERROR( cudaMemcpy( dev_a, a, (sizeof(double)*N*N), cudaMemcpyHostToDevice ));
    HANDLE_ERROR( cudaMemcpy( dev_b, b, (sizeof(double)*N*N), cudaMemcpyHostToDevice ));
    HANDLE_ERROR( cudaMemcpy( dev_c, c, (sizeof(double)*N*N), cudaMemcpyHostToDevice ));

    HANDLE_ERROR(cudaEventCreate(&start));
    HANDLE_ERROR(cudaEventCreate(&stop));
    HANDLE_ERROR(cudaEventRecord(start, 0));

    dgemm_def<<<1,1>>>( dev_a, dev_b, dev_c );

    HANDLE_ERROR(cudaEventRecord(stop, 0));
    HANDLE_ERROR(cudaEventSynchronize(stop));
    float elapsedTime;
    HANDLE_ERROR(cudaEventElapsedTime(&elapsedTime, start, stop));
    printf("Elapsed time = %3.1f\n", elapsedTime);

/*    HANLDE_ERROR( cudaMemcpy(&a, dev_a, sizeof(double)*N*N), cudaMemcpyDeviceToHost );
    HANLDE_ERROR( cudaMemcpy(&b, dev_b, sizeof(double)*N*N), cudaMemcpyDeviceToHost );
    HANLDE_ERROR( cudaMemcpy(&c, dev_c, sizeof(double)*N*N), cudaMemcpyDeviceToHost );  */

    HANDLE_ERROR( cudaEventDestroy(start) );
    HANDLE_ERROR( cudaEventDestroy(stop) );
    cudaFree(dev_a);
    cudaFree(dev_b);
    cudaFree(dev_c);
    return 0;

}
